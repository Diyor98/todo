from django.urls import path

from todo.views import ListTodo, DetailTodo

urlpatterns = [
    path('<int:pk>/', DetailTodo.as_view()),
    path('',ListTodo.as_view()),
]